package com.example.terp.qrproxy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    public TextView scan_result_element;
    public TextView wear_ip_element;
    public TextView wear_port_element;
    public String last_qr_scanned = "UNINITIALIZED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView t = new TextView(this);

        scan_result_element =(TextView)findViewById(R.id.scan_result);

    }


    public void scanBarcode(View view) {
        new IntentIntegrator(this).initiateScan();
    }


    public void SendCode(View view) {
        Toast.makeText(this, last_qr_scanned, Toast.LENGTH_LONG).show();
        BackgroundConnection t = new BackgroundConnection();
        EditText _ip = (EditText)findViewById(R.id.wear_adress);
        EditText _port = (EditText)findViewById(R.id.wear_port);

        String ip = _ip.getText().toString();
        String port = _port.getText().toString();
        t.execute(last_qr_scanned, ip, port);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
                last_qr_scanned = result.getContents();
                Toast.makeText(this, "Scanned: " + last_qr_scanned, Toast.LENGTH_LONG).show();
                scan_result_element.setText(last_qr_scanned);
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
