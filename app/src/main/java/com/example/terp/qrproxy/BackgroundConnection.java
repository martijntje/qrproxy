package com.example.terp.qrproxy;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Random;

/**
 * Created by terp on 3-11-16.
 */

public class BackgroundConnection extends AsyncTask<String, Void, String>{


    protected String doInBackground(String... arguments) {

        Log.i("MSG:",arguments[0]);
        Log.i("IP:",arguments[1]);
        Log.i("PORT:",arguments[2]);

        String msg = arguments[0];
        String ip  = arguments[1];
        int port   = Integer.parseInt(arguments[2]);

        try{

            // Creating new socket connection to the IP (first parameter) and its opened port (second parameter)
            Socket s = new Socket(ip, port); // timeout happens after 2 minutes
            // Initialize output stream to write message to the socket stream
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out.write(msg);
            out.write("\n");
            out.flush();

            //String response1 = in.readLine();
            s.close();

        }
        catch(Exception ex){
            //Handle exceptions
            Log.i("Catch{","Something went wrong");
            ex.printStackTrace();
            return "failure";
        }
        return "succes";
    }

    protected void onPostExecute(String result){
        String TAG = "Result of connection";
        /* output result */
        Log.i(TAG,result);

    }
}